FROM nginx:stable-alpine3.17-slim

WORKDIR /sigstackfaultcom/
RUN apk add libc6-compat
RUN apk add make py3-pip npm
RUN pip install virtualenv
COPY Makefile .
COPY package.json .
COPY requirements.txt .
RUN rm /etc/nginx/conf.d/default.conf
COPY sigstackfault.conf /etc/nginx/conf.d/
COPY src/ src/
RUN make

VOLUME /etc/letsencrypt
EXPOSE 80
EXPOSE 443