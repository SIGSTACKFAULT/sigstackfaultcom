var projects_container = document.getElementById("projects-container");
var projects_spinner = document.getElementById("projects-spinner");
var projects_icon_titles = {
    "python": "Python",
    "uikit": "Uikit",
    "html5": "HTML5",
    "jinja": "Jinja"
};
var projects_data = [
    {
        "name": "SbTeX",
        "icons": ["python"],
        "description": "Helper tool for an ARG; don't ask what it does.<br>Not <em>actually</em> related to TeX, but I was like 15 okay",
        "url": "https://gitlab.com/SbTeX/SbTeX"
    }, {
        "name": "arghelper",
        "icons": ["python", "discord"],
        "description": "Discord bot to help with solving <a href=\"https://en.wikipedia.org/wiki/Alternate_reality_game\">Alternate Reality Games</a>",
        "url": "https://gitlab.com/SIGSTACKFAULT/arghelper"
    }, {
        "name": "sigstackfault.com",
        "icons": ["uikit", "jinja"],
        "description": "This website!",
        "url": "https://gitlab.com/SIGSTACKFAULT/sigstackfaultcom"
    }, {
        "name": "ROBOT9000",
        "icons": ["python", "discord"],
        "description": "Discord implementation of xkcd's ROBOT9000",
        "url": "https://gitlab.com/SIGSTACKFAULT/ROBOT9000",
    }, {
        "name": "Fleetwright Web Designer",
        "icons": ["typescript", "vuedotjs"],
        "description": "Editor & VTT for a TTRPG/Wargame",
        "url": "https://gitlab.com/SIGSTACKFAULT/fleetwright-web-designer",
    }
];

function projects_add(obj) {
    // create the card itself
    card = document.createElement("div");
    card.classList.add("projects-card");
    card.classList.add("uk-card");
    card.classList.add("uk-card-body");
    card.classList.add("uk-card-default");
    card.classList.add("uk-margin-right");
    card.classList.add("uk-margin-bottom");
    card.classList.add("uk-animation-slide-bottom-medium");

    // card's title
    card_title = document.createElement("h3");
    card_title.innerHTML = "<a href=\"" + obj.url + "\">" + obj.name + "</a>";
    card_title.classList.add("uk-card-title");

    card_icons_wrapper = document.createElement("div");
    card_icons_wrapper.classList.add("projects-icons-wrapper");
    card_icons_wrapper.classList.add("uk-margin-left");
    card_icons_wrapper.classList.add("uk-float-right");
    card_icons_wrapper.classList.add("uk-visible@s");
    card_title.appendChild(card_icons_wrapper);

    // add icons, mostly for what language the project uses
    for (var icon of obj.icons) {
        img = document.createElement("img");
        img.setAttribute("src", "static/icons/" + icon + ".svg");
        img.setAttribute("uk-tooltip", projects_icon_titles[icon]);
        img.classList.add("projects-icon");
        card_icons_wrapper.appendChild(img);
    }

    // card's content
    card_content = document.createElement("p");
    card_content.classList.add("uk-text-small");
    card_content.innerHTML = obj.description;

    // actually put the title and content into the card
    card.appendChild(card_title);
    card.appendChild(card_content);

    projects_container.appendChild(card);
}


// i'd kill for a better way to do a for loop with a delay
var projects_i = 0;
var projects_interval = setInterval(function () {
    // don't remove spinner until the first card actually comes in
    projects_spinner.remove();

    projects_add(projects_data[projects_i]);
    projects_i += 1;
    if (projects_i >= projects_data.length) clearInterval(projects_interval);
}, 500);
