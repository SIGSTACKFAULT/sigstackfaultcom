from functools import partial
import http.server
import os
import multiprocessing
import time

import easywatch

PORT = 8000
BUILD_DIR="build"

httpd = http.server.HTTPServer(
    server_address=('',PORT),
    RequestHandlerClass=partial(http.server.SimpleHTTPRequestHandler, directory=BUILD_DIR)
)


def serve():
    try:
        print(f"serving from {BUILD_DIR}...")
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass


def handler(event_type, src_path):
    print(f"detected change in {src_path}")
    os.system("make")


serve_proc = multiprocessing.Process(target=serve)
serve_proc.start()

print("serving at http://localhost:8000")

try:
    print("watching src...")
    easywatch.watch("src", handler)
except KeyboardInterrupt:
    print("\n")
    serve_proc.kill()